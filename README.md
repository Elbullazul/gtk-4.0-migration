# GTK Divergences between GTK 3.x and GTK 4
This is a file with differences between the `_common.scss` files between gtk+4.0 and gtk+3.0 of the [Adwita theme](https://github.com/GNOME/gtk/tree/master/gtk/theme/Adwaita)

### General remarks
- Direct child widgets are routinely called with the direct descendant '>' selector, probably to limit the rule scope and prevent it cascading down to other unrelated child widgets
- Many `:backdrop` rules have been droppped, mostly in widgets where having a backdrop status was not necessary or was confusing
- The GNOME team seems to have reduced the amount ot color variables used. Good job!

### Differences in code
- No more wildcard on file start (deprecated)
- `dnd` widget and icon sizing classes
```scss
    dnd {
    color: $fg-color;
    }

    .normal-icons {
    -gtk-icon-size: 16px;
    }

    .large-icons {
    -gtk-icon-size: 32px;
    }
```

- new property `gtk-icon-filter` controls icon opacity
```scss
spinner:disabled,
arrow:disabled,
scrollbar:disabled,
check:disabled,
radio:disabled,
treeview.expander:disabled { -gtk-icon-filter: opacity(0.5); }
```

- removed .gtkstyle-fallback styling (deprecated?)
- added `>` selectors to a lot of nodes (probably to reduce rule scope). This is to avoid applying rule to indirect `text` children of `textview`
    ex. `textview text` is now `textview > text`
- adds `:drop(active)` pseudo-class to `textview` and `iconview`
```scss
textview {
  > text {
    ...
  }

  &:drop(active) {
    caret-color: $drop_target_color;
  }
}
```
- added `outline` property to `iconview`
- new (?) `dndtarget` widget for `iconview`, with `:drop(active)` support
```scss
iconview {
  ...
  &:drop(active) { box-shadow: none; }
  > dndtarget:drop(active) { border: 1px solid $selected_borders_color; }
}
```

- added `outline` property to `rubberband`
- `.content-view .tile` rule replaced with `gridview`, with each 'tile' becoming a `child` widget. Weirdly, border-spacing for `child` has to be done in `child > box`. `child` has outline support. Also, `.content-view .tile` is found later on in the file
```scss
gridview {
  > rubberband { @extend rubberband; }
  > child {
    box { //cells
      border-spacing: 8px; //label separation
      margin: 12px;
    }
  }
}
```

- new widget `coverflow cover`
```scss
coverflow cover {
  color: $text_color;
  background-color: $base_color;
  border: 1px solid black;
}
```

- labels support `outline` property
- `assistant` widget is now named `window.assistant`
- sizing for about dialog icons
```scss
window.aboutdialog image.large-icons {
  -gtk-icon-size: 128px;
}
```

- loading animation `@keyframes spin` uses `transform` instead of `-gtk-icon-transform`
- added `outline` property to `entry` widget
- new widgets for `entry` widget: `entry > text > placeholder` represents placeholder text and `entry > text > block-cursor`, which I believe replaces the `caret-*` properties
- new `:focus-within` pseudo-class for `entry`. Seemingly replaces `:focus` call when activated, probably because widgets inside the entry can also inject this pseudo-class
- `entry` now seems uses `entry > text > selection` for text selection rules; previously `entry selection` (seems to not be the case with `.error` and `.warning` classes)
- new class `.password` for password entries and `image.caps-lock-indicator` widget for caps-lock indicator in a password entry
```scss
&.password image.caps-lock-indicator {
    color: mix($backdrop_fg_color, $backdrop_base_color, 80%);
}
```
- **Remidner**: `entry` widget supports `:drop(active)` pseudo-class
- the `progress` widget for `entry` is now under the `trough` widget:
*previously*
```scss
entry progres { ... }
```

*current*
```scss
entry progress > trough > progress { ... }
```

**Note**: margin-bottom is still aplied to `entry > progress`

- **Reminder**: `entry` widget can contain `button` widgets
- `.entry-tag` class renamed as `editablelabel > stack > text`
- `-gtk-gradient()` function seems to be deprecated, usage replaced with `radial-gradient()` standard CSS function
- `-gtk-icon-effect` property is replaced with `-gtk-icon-filter`
*previously*
```scss
-gtk-icon-effect: highlight;
```

*current*
```scss
-gtk-icon-filter: brightness(1.2);
```

- `:checked` pseudo-class can be used with `:hover` and `:active` pseudo-classes.
- `button.flat` has problem with border transition animations when grouped with other buttons. Fix: apply transition to `:hover` status
- replace class name in `.stack-switcher > button` with widget name: `stackswitcher > button`
- removed special padding for `button.image-button` and `button.text-button` classes for stack switcher buttons
- `button.file` separator rule removed (presumably uses `separator` rules)
- `.inline-toolbar button` and `.primary-toolbar button` rules removed
- `menubutton.circular` added to the `button.circular` rule
- `.inline-toolbar toolbutton > button` and `toolbar.inline-toolbar toolbutton` are deprecated (based on comment, it is the +|- buttons in inline-toolbars)
- modification to `.linked` rules
- `menubutton`, `dropdown`, `colorbutton`, `fontbutton` and `filechoosebutton` appear in `.linked` rules
- `.menuitem.button.flat` class deprecated and removed from `modelbutton.flat` rules
- new rule for `.toolbar button` added
```scss
/* oldstyle toolbar buttons */

.toolbar button {
  margin: 1px;
  @extend %undecorated_button;

  &:hover { @include button('hover'); }
  &:active { @include button('active'); }
  &:disabled { @include button('insensitive'); }
  &:backdrop { @include button('backdrop'); }
  &:backdrop:disabled { @include button('backdrop-insensitive'); }
}
```

- added rules to list button for `.suggested-action` and `.destructive-action` classes
- `:link` pseudo-class deprecated, using `link` widget now. To reproduce the underline, use
```scss
link {
  text-decoration: underline;
  ...
}
```

- outline property added to `link` widget
- `link > label` no longer has to extend styling for `link` (only use the `text-decoration: underline` rule)
- `spinbutton entry` replaced with `spinbutton > text`. This seems to point that spinbutton does not have an `entry` widget anymore
- `spinbutton button` replaced with `spinbutton > button.image-button.up:not(.flat)` and `spinbutton > button.image-button.down:not(.flat)`. The `:not` allows it to override rules inherited from list button styling
- previous two rule changes also apply to `.osd spinbutton`
- `spinbutton > button.image-button.up:not(.flat)` and `spinbutton > button.image-button.down:not(.flat)` not used with `.vertical` class (still uses `button.up` and `button.down`)
- `spinbutton` in `treeview` uses `> text` instead of `entry`
- `dropdown` widget added to `combobox` rule
- `dropdown > popover.menu.background > contents` is used to set combo menu item padding:
```scss
dropdown > popover.menu.background > contents { padding: 0; } //allow search entries with no margin
```
- added rule for `combobox`/`dropdown` menu padding. Seems like in GTK 4, this menu will use a listview in a popover. Added `.dropdown-searchbar` for combo menus with search entry
```scss
// align menu labels with the button label
dropdown,
combobox {
  ...
  // align menu labels with the button label
  > popover.menu > contents modelbutton {
    padding-left: 9px;
    padding-right: 9px;
  }
  ...
  // newstyle
  popover {
   margin-top: 6px;
   padding: 0;

   listview {
      margin: 8px 0;

      & > row {
         padding: 8px;

         &:selected {
           outline-color: $alt_focus_border_color;
           color: $text-color;
           background-color: $menu_selected_color;
         }
      }
   }

    // drodowns with searchboxes on top
    .dropdown-searchbar {
      padding: 6px;
      border-bottom: 1px solid $borders_color;
    }
  }
}
```

- `appchooserbutton` is now also included in `.linked` rules
- `-GtkWidget-window-dragging` property deprecated
- class `.toolbar` added to the `toolbar` rule
```scss
.toolbar,
toolbar {
    ...
}
```

- reduced margins on toolbar separators
- `.location-bar` rule merged with `searchbar > revealer > box`
- `.selection-mode` rules removed, except for `check` and `radio` widgets
- `button.titlebutton` or `.titlebutton` replaced with `windowcontrols button` and `windowcontrols menubutton`
```scss
windowcontrols {
    button,
    menubutton {
        min-height: 26px;
        min-width: 26px;
        margin: 0;
        padding: 0;
    }

    menubutton button {
        min-height: 20px;
        min-width: 20px;
        margin: 0;
        padding: 4px;
    }
}
```

- removed `separator.titlebutton` rule, presumably replaced with `windowhandle` widget
```scss
headerbar {
  > windowhandle > box {
    &,
    > box.start,
    > box.end {
      border-spacing: 6px;
    }
  }
}
```

- added rule to add margins to headerbar controls to avoid having them fill up vertical space
```scss
headerbar {
  // add vertical margins to common widget on the headerbar to avoid them spanning the whole height
  entry,
  spinbutton,
  separator:not(.sidebar),
  button,
  menubutton {
    margin-top: 6px;
    margin-bottom: 6px;
  }

  // Reset margins for buttons inside menubutton
  menubutton > button {
    margin-top: 0px;
    margin-bottom: 0px;
  }   
}
```

- `.background .titlebar` rule removed, as well as the following:
```scss
.background.tiled .titlebar
.background.tiled-top .titlebar
.background.tiled-right .titlebar
.background.tiled-bottom .titlebar
.background.tiled-left .titlebar
.background.maximized .titlebar
.background.solid-csd .titlebar

headerbar { // headerbar border rounding

  window separator:first-child + &,
  window &:first-child { &:backdrop, & { border-top-left-radius: 7px; }}
  window &:last-child { &:backdrop, & { border-top-right-radius: 7px; }}

  window stack & { // tackles the stacked headerbars case
    &:first-child, &:last-child {
      &:backdrop, & {
        border-top-left-radius: 7px;
        border-top-right-radius: 7px;
      }
    }
  }

  window.tiled &,
  window.tiled-top &,
  window.tiled-right &,
  window.tiled-bottom &,
  window.tiled-left &,
  window.maximized &,
  window.fullscreen &,
  window.solid-csd & {}
}
```

- **Remark**: Apparently, dev versions of apps are supposed to have a different headerbar
```scss
window.devel {
  headerbar.titlebar {
      ...
  }
}
```

- `.path-bar` class replaced by widget name `pathbar`
- removed `filechooser .path-bar.linked > button` rule specific to file chooser
- removed following properties from `treeview.view` widget
```scss
-GtkTreeView-horizontal-separator
-GtkTreeView-grid-line-width
-GtkTreeView-grid-line-pattern
-GtkTreeView-tree-line-width
-GtkTreeView-tree-line-pattern
-GtkTreeView-expander-size
```

- outline property added to `treeview.view` widget with `:selected:focus` pseudo-classes
- **Reminder**: `treeview.view` supports `:drop(active)` pseudo-class
- `dndtarget:drop(active)` rule added to `treeview.view` widget
- added min-width and min-height properties to `treeview.view.expander`
```scss
treeview.view {
  ...
  &.expander {
    // GtkTreeView uses the larger of the expander’s min-width and min-height
    min-width: 16px;
    min-height: 16px;
  }
  ...
}
```

- `treeview.view > header > button` instead of `treeview.view header button`. Also adds `sort-indicator` to replace `arrow` widgets in treeview header buttons
```scss
sort-indicator {
    &.ascending {
        -gtk-icon-source: -gtk-icontheme('pan-up-symbolic');
    }
    &.descending {
        -gtk-icon-source: -gtk-icontheme('pan-down-symbolic');
    }

    min-height: 16px;
    min-width: 16px;
    }
}
```

- new rules for `popover.background` widget
- new rule `popover.background > contents`. Requires the following snippet to clear inherited rules from `list`
```scss
popover.background {
  > contents {
    padding: 8px;   // menuitem spacing
    border-radius: $popover_radius;

    > list,
    > .view,
    > toolbar {
      border-style: none;
      background-color: transparent;
    }

    separator {
      background-color: mix($bg_color, $borders_color, 30%);
      margin: 3px;
    }

    list separator { margin: 0; }
  }
}
```

- `popover.background.touch-selection`
- new `magnifier` widget
- `menu` widget and `.menu` class replaced with `popover.menu`
- new rules for `popover.menu .box-inline` padding, `popover.menu .box-inline button.image-button-label` styling, `popover.menu .box-circular-buttons` padding and `popover.menu .box-circular-buttons button.circular.image-button.model` styling
- `popover.menu` content padding set in `popover.menu.background > contents`
- `accelerator` widget can be nested in popovers
- `popover.menu modelbutton` is used instead of `menuitem` or `.menubutton`
- menu title changed to `popover.menu label.title`
- `-GtkWidget-window-dragging` property removed from `menubar` widget
- `menubar menuitem` changed to `menubar > item`
- nested submenus for `menubar` defined as follows:
```scss
menubar {
  ...
  // content padding
  & > item popover.menu.background > contents {
    padding: $menu-margin;
  }
  //nested submenus
  & > item popover.menu popover.menu {
    padding: 0 0 4px 0;
  }
  & > item popover.menu.background popover.menu.background > contents {
    margin: 0;
    border-radius: $popover_radius; //including top
  }
}
```
**Remark**: menu item **padding** seems to be covered by modelbutton padding and menu item **margin** (space between menu items) managed by popover contents padding

- added outline property to `notebook tab` widget
- `notebook > header > tabs > tab` can now be addressed as `notebook > header > tab`, at least for transitions, padding and margins
- transparent outline for `:not(:checked)` tabs
- `-GtkScrollbar-has-backward-stepper` and `-GtkScrollbar-has-forward-stepper` properties removed. Scrollbar stepper buttons have been removed? (uncertain)
- `scrollbar slider` changed for `scrollbar > range > trough > slider` in `.fine-tune` and `.overlay-indicator` scrollbars. Also applies to `.vertical` and `.horizontal` scrollbars
- `scrollbar.fine-tune` changed for `scrollbar > range.fine-tune`
- `switch` font-weight set to **bold** and font-size to *smaller*. Added outline property
- `switch` widgets in headerbars have a darker fill background
- `switch:[pseudo-class] slider` replaced by `switch:[pseudo-class] > slider`
- `.content-view:not(list) [widget]` replaced by `.content-view .tile [widget]:not(list)`
- removed `check/radio` margin in popovers, seems like GTK will manage placement in menus now
- removed rules for `popover check/radio` and `menu menuitem check/radio`
- `-gtk-recolour()` function allows recolouring of compatible assets (or icons?)
- sizing for `check/radio` seems to no longer be managed with min-height/min-width, but with `-gtk-icon-size`. Ex:
*previously*
```scss
// sizing at 14px
check, radio {
  min-height: 14px;
  min-width: 14px;
}
```

*currently*
```scss
check, radio { -gtk-icon-size: 14px; }
```

- removed `check/radio` custom animations (use CSS ones instead?)
- darker backgroud color for `scale trough` in headerbars
- outline property added to `scale` widget
- `scale trough` renamed to `scale > trough`
- `scale highlight` renamed to `scale > trough > highlight` and `scale fill` renamed to `scale > trough > fill`
- `scale slider` renamed to `scale > trough > slider`
- `progressbar trough` renamed to `progressbar > trough`
- `progressbar progress` renamed to `progressbar > trough > progress`
- `levelbar block` renamed to `levelbar trough > block`
- `printdialog` widget replaced with `window.dialog.print`
- `printdialog paper` replaced with `window.dialog.print paper`
- added `window.dialog.print drawing`, seemingly for text drawn on the `paper` widget
- `frame > border` renamed `frame`
- border-radius property added to `frame` widget
- `label` widget in frame can now be addressed with `frame > label`
- `scrolledwindow viewport.frame` rule removed (apprently, no more problems with double frame borders on viewports)
- `listview` widget added to `list` widget rules:
*previously*: `list { ... }`
*currently*: `listview, list { ... }`

- outline property added to `row` widget
- new widgets `columnview` and `treeexpander`:
```scss
columnview {
  // move padding to child cells
  > listview > row {
    padding: 0;

    // align horizontal sizing with header buttons
    > cell {
      padding: 8px 6px;

      &:not(:first-child) {
        border-left: 1px solid transparent;
      }
    }
  }

  // make column separators visible when :show-column-separators is true
  &.column-separators > listview > row > cell {
    border-left-color: $_treeview_borders_color;
  }

  // shrink vertically for .data-table
  &.data-table > listview > row > cell {
    padding-top: 2px;
    padding-bottom: 2px;
  }
}

treeexpander {
  border-spacing: 4px;
}
```

- new control `columnview` apparently used for data tables:
```scss
/********************************************************
 * Data Tables                                          *
 * treeview like tables with individual focusable cells *
 * https://gitlab.gnome.org/GNOME/gtk/-/issues/2929     *
 ********************************************************/

columnview row:not(:selected) cell editablelabel:not(.editing):focus-within {
  outline: 2px solid $focus_border_color;
}

columnview row:not(:selected) cell editablelabel.editing:focus-within {
  outline: 2px solid $selected_bg_color;
}

columnview row:not(:selected) cell editablelabel.editing text selection {
  color: $selected_fg_color;
  background-color: $selected_bg_color;
}
```

- new class `.rich-list` for large lists with lots of content
```scss
/*******************************************************
 * Rich Lists                                          *
 * Large list usually containing lots of widgets       *
 * https://gitlab.gnome.org/GNOME/gtk/-/issues/3073    *
 *******************************************************/


.rich-list { /* rich lists usually containing other widgets than just labels/text */
  & > row {
    padding: 8px 12px;
    min-height: 32px; /* should be tall even when only containing a label */

    & > box {
      border-spacing: 12px;
    }
  }
}
```

- removed `.app-notification.frame` special use class
- added border-spacing property
- new expander `expander-widget`
```scss
expander-widget {
  @include focus-ring("> box > title"); // outline

  > box > title {
    transition: $focus_transition;
    border-radius: $button_radius;

    &:hover > expander {
      color: lighten($fg_color,30%); //only lightens the icon
    }
  }
}

.navigation-sidebar,
placessidebar,
stackswitcher,
expander-widget {
  &:not(decoration):not(window):drop(active):focus,
  &:not(decoration):not(window):drop(active) {
    box-shadow: none;
  }
}
```

- `calendar.header` replaced with `calendar > header`
- `calendar.button` replaced with `calendar > header > button`
- calendar grid items now accessible with `calendar > grid`
- `calendar.highlight` widget removed
- calendar day name labels accessible with `calendar > grid > label.day-name`
- calendar week number labels accessible with `calendar > grid > label.week-number`
- calendar other month label accessible with `calendar > grid > label.day-number.other-month`
- calendar today label accessible with `calendar > grid > label.today`
- `messagedialog` widget removed in favor of `window.dialog.message`
- vertical messageboxes seem to have been added:
```scss
window.dialog.message {
  ...
  box.dialog-vbox.vertical {
    border-spacing: 10px;
  }
  ...
}
```

- messagebox title now accessible through `window.dialog.message label.title`
- file chooser button seems to have some border spacing issues:
```scss
filechooserbutton>button>box {
  border-spacing: 6px;
}
```
**Reminder**: `filechooserbutton` support `:drop(active)` pseudo-class

- `listview.view` added as a child of `.sidebar` controls:
```scss
.sidebar {
  listview.view {
    ...
  }
  ...
}
```

- `stacksidebar` not considered a regular list anymore, needs separate rules
```scss
stacksidebar {
  //not a regular list
  list.separators:not(.horizontal) > row:not(.separator) {
    border-bottom: none;
  }
  row {
    padding: 10px 4px;

    > label {
      padding-left: 6px;
      padding-right: 6px;
    }

    &.needs-attention > label {
      @extend %needs_attention;
       background-size: 6px 6px, 0 0;
    }
    &:selected {
      background-color: $menu_selected_color;
      border-radius: $menu_radius;
      color: $fg_color;
    }
    &:selected:hover:dir(ltr), &:selected:hover:dir(rtl) {
      background-color: darken($menu_selected_color,5%);
    }
    &:focus:focus-visible {
      outline-width: 0;
      background-color: $selected_bg_color;
      color: $selected_fg_color;
    }
    &.activatable:active, &.activatable:selected:active {
      box-shadow: none; // #3413
    }
  }
}
```

- new `.navigation-sidebar` class:
```scss
.navigation-sidebar {
  padding: $menu-margin 0; //only vertical padding. horizontal row size would clip

  > separator {
    margin: $menu-margin;
  }

  > row {
    min-height: 36px;
    padding: 0 8px;
    border-radius: $menu-margin;
    margin: 0 $menu-margin 2px;

    &:hover,
    &:focus-visible:focus-within {
      background-color: darken($menu_selected_color, 5%);
    }

    &:selected {
      background-color: $menu_selected_color;
      color: inherit;

      &:hover {
        background-color: darken($menu_selected_color,5%);
      }

      &:focus-visible:focus-within {
        outline-width: 0;
        color: $selected_fg_color;
        background-color: $selected_bg_color;
        &:hover { background-color: darken($selected_bg_color,10%); }
      }
    }

    &:disabled { color: $insensitive_fg_color; }
  }
}
```

- `placessidebar` now uses `.navigation-sidebar` class instead of `viewport.frame`
```scss
placessidebar {
  .navigation-sidebar > row {
    // Needs overriding of the `.navigation-sidebar > row` padding
    padding: 0;

    // Using margins/padding directly in the SidebarRow
    // will make the animation of the new bookmark row jump
    > revealer { padding: 0 14px; }
    ...
  }
}
```

- apparently new widget `revealer` to replace `arrow.down`
- removed a rule for `placesview` and potential revealer border spacing issues:
```scss
placesview {
  .server-list-button > image {
    transition: 200ms $ease-out-quad;
    -gtk-icon-transform: rotate(0turn);
  }

  .server-list-button:checked > image {
    transition: 200ms $ease-out-quad;
    -gtk-icon-transform: rotate(-0.5turn);
  }

  row.activatable:hover { /* empty */ }

  > actionbar > revealer > box  > box{
    border-spacing: 6px;
  }
}
```

- new `video` widget for video playback
```scss
video {
  & image.osd {
    min-width: 64px;
    min-height: 64px;
    border-radius: 32px;
  }
  background: black;
}
```

- `tooltip *` wildcard rule removed, as well as `tooltip decoration` rule
- added `tooltip > box` border spacing:
```scss
tooltip {
  > box {
    border-spacing: 6px;
  }
}
```

- added outline property and transitions to `colorswatch`
- added `colorswatch.activatable` widget class (usage?)
- new widget `plane`, using outline property and transitions (future usage?)
- new rule `.content-view .tile`, `.content-view rubberband` readdressed as `.content-view > rubberband`

### Important
- `decoration` widget replaced with `window`
- rounded borders now possible with `window` widget, but a border around equal to the radius seems to be required for it to work
```scss
// 4px radius
window {
  // lamefun trick to get rounded borders regardless of CSD use
  border-width: 4px;

  &.csd { border-radius: 4px; }
}
```

- window manager now draws window shadows, `window` only defines 'border shadows'
- `decoration` margin rule for resizing area around windows is now removed (probably handled by WM)
- `window` widget not used by itself, always as one of the following:
```scss
window.csd          // radius is defined here
window.popup        // all rounded borders (?), no shadow
window.dialog.message // (previously messagedialog.csd decoration)
window.solid-csd    // no padding required this time
window.maximized    // border radius = 0 and now shadows
window.fullscreen   // border radius = 0 and now shadows
window.tiled
window.tiled-top
window.tiled-left
window.tiled-right
window.tiled-bottom // border radius = 0, transparent shadow (seems temporary)
window.ssd          // just borders, wm (mutter) draws shadows
```

- `tooltip.csd` has radius and no shadow
- `cursor-handle` widget for touch selection now has padding right/left/top/bottom properties, as well as min height/width
- `.context-menu` class removed
- new widget `shortcuts-section`
- possible issues with shortcuts search results in keyboard shortcuts window
```scss
.shortcuts-search-results {
  margin: 20px;
  border-spacing: 24px;
}
```

- new `shortcut` widget:
```scss
// shortcut window keys
shortcut {
  border-spacing: 6px;
}
```
- `.keycap` class replaced woth `shortcut > .keycap`
- circular stackswitcher with `stackswitcher.circular`
- new rules for emoji pane
```scss
popover.emoji-picker > contents {
  padding: 0;
}

.emoji-searchbar {
  padding: 6px;
  border-spacing: 6px;
  border-bottom: 1px solid $borders_color;
}

.emoji-toolbar {
  padding: 6px;
  border-spacing: 6px;
  border-top: 1px solid $borders_color;
}

...

emoji-completion-row > box {
  border-spacing: 10px;
  padding: 2px 10px;
}

emoji-completion-row:focus,
emoji-completion-row:hover {
  background-color: $selected_bg_color;
  color: $selected_fg_color;
}

emoji-completion-row emoji:focus,
emoji-completion-row emoji:hover {
  background-color: $menu_selected_color;
}

popover.entry-completion > contents {
  padding: 0;
}
```

- `popover.emoji-picker .emoji` replaced with `popover.emoji-picker emoji`
- possible spacing problem with statusbar
```scss
statusbar {
  padding: 6px 10px 6px 10px;
}
```

- still icnludes `menubutton arrow` code
```scss
menubutton {
  arrow {
    min-height: 16px;
    min-width: 16px;
    &.none {
      -gtk-icon-source: -gtk-icontheme('open-menu-symbolic');
    }
    &.down {
      -gtk-icon-source: -gtk-icontheme('pan-down-symbolic');
    }
    &.up {
      -gtk-icon-source: -gtk-icontheme('pan-up-symbolic');
    }
    &.left {
      -gtk-icon-source: -gtk-icontheme('pan-start-symbolic');
    }
    &.right {
      -gtk-icon-source: -gtk-icontheme('pan-end-symbolic');
    }
  }
}
```